<%--
  Created by IntelliJ IDEA.
  User: dawos
  Date: 24.03.2019
  Time: 11:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Error Page jsp</title>
    <style>
        <%@include file="../../css/login.css"%>
    </style>
</head>
<body>
<%@include file="../pageHeader.jsp" %>
<%
    response.setStatus(401);
%>
<div style="font-size: 30px; color: red;">
    Nieporpawny login lub hasło.
</div>
<%@include file="../pageFooter.jsp" %>

</body>
</html>
