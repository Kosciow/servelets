<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: dawos
  Date: 30.03.2019
  Time: 09:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
    <style><%@include file="../css/exercises.css"%></style>
</head>
<body>
<div id="header">
    <h1 class="title-class">
        <p>Dziesiaj jest : <%=java.time.LocalDateTime.now().toString()%></p>
        <br>
        <p><%=10+200%></p>
    </h1>
</div>
<h1>
    Skryptlet:
</h1>
<p>
    <%
        for (int i=0;i<10;i++){
            out.print(i);
        }
    %>
</p>

<table class="title-class">
    <thead>
    <tr>
        <td>Imię</td>
        <td>Nazwisko</td>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>Tomek</td>
        <td>Pierwszy</td>
    </tr>
    <tr>
        <td>Ewa</td>
        <td>Kot</td>
    </tr>

    <%
        for (int i=0;i<3;i++){
            out.print("<tr><td>"+i+"</td><td>"+i*i+"</td></tr>");
                    }
    %>

    <%for (int i=0;i<3;i++){%>
    <tr>
        <td><%= i%></td>
        <td><% out.print(i); %></td>
    </tr>
    <%}%>

    </tbody>
</table>

<h1>
    Dyrektywy
</h1>
<%
    LocalDateTime.now();

%>
<br>
<%! private int visitCount = 0;%>
Liczba wyświetleń: <%=visitCount%>
<%
    visitCount++;
%>
<h1>Zadanie 1</h1><br><br>
<%
    String liczba1 = request.getParameter("liczba1");
    String liczba2 = pageContext.getRequest().getParameter("liczba2"); //drugi sposób

    if(liczba1 !=null || liczba2 !=null) {
        int first = Integer.parseInt(liczba1);
        int second = Integer.parseInt(liczba2);
        int result = first + second;
        out.print("Wynik dodawania: " + result);
        //out.print(request.getRequestURL().toString());
    }else{
        out.print("Podaj poprawne URL");
    }
%>

<h1>Zadanie 2</h1>
<%
    //wyciagniecie atrybutu z requestu
    String productName = request.getParameter("productName");
    //drugi sposób na wyciągnięcie parametru z requesta
    String productCount = pageContext.getRequest().getParameter("productCount");
    if(productName==null || productCount==null){
        out.print("Podaj parametry");
    }else {
        int productCountInt = Integer.parseInt(productCount);



        //wyciagniecie map produktów z sesji
        Map<String, Integer> productsMap = (Map<String, Integer>) session.getAttribute("productsMap");

        //obsluzenie przypadku, gdy w sesji nie ma jeszcze mapy z produktami
        if (productsMap == null) {
            HashMap<String, Integer> newProductsMap = new HashMap<>();
            newProductsMap.put(productName, productCountInt);
            session.setAttribute("productsMap", newProductsMap);
        } else {
            productsMap.put(productName, productCountInt);
            session.setAttribute("productsMap", productsMap);
        }
    }



%>
<%
    Map<String,Integer> productsMapToDisplay=(Map<String, Integer>) session.getAttribute("productsMap");
    if(productsMapToDisplay==null){
        out.print("Nie ma żadnych produktów w liście");
    }else{
        for (Map.Entry<String, Integer> mapEntity : productsMapToDisplay.entrySet()){
            out.print(mapEntity.getKey()+" "+mapEntity.getValue());
        }
    }
%>


<div style="color: green"><h1>

    Cwieczeni Cwiczenia

</h1>
    INCLUDE
    <jsp:include page="/index2.jsp"/>

    BEAN
    <jsp:useBean id="calculatorBean"
    class="main.demojsp.MyCalculatorBean"
    scope="session"/>

    <jsp:setProperty name="calculatorBean" property="multiplyNumber" value="10"/>
    <jsp:setProperty name="calculatorBean" property="addingNumber" value="2"/>

    <%
        int add = calculatorBean.add(100);
        int multiply = calculatorBean.multiply(100);

        out.print("<br>");
        out.print(add);
        out.print("<br>");
        out.print(multiply);
    %>


    <p>NORMALNY TEXT</p>
    <a href="http://www.google.pl">GOOGLE</a></div>
</body>
</html>
