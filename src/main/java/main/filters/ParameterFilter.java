package main.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(urlPatterns = "/*") // "/*" - dotyczy wszystkich servletow
public class ParameterFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;

        Enumeration<String> attributeNames = httpRequest.getAttributeNames();

        while (attributeNames.hasMoreElements()){
            String attributeName = attributeNames.nextElement();
            String parameterValue = httpRequest.getParameter(attributeName);
            System.out.println(parameterValue);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
